from django.db import models

# Create your models here.
class Matkul(models.Model):
    mata_kuliah = models.CharField(max_length=50,default='')
    dosen = models.CharField(max_length=100,default='')
    sks = models.IntegerField()
    tahun = models.IntegerField()
    kelas = models.CharField(max_length=50,default='')
