from django.shortcuts import render, redirect
from .models import Matkul
from .forms import matkulForm

# Create your views here.
def home(request):
    return render(request, 'homePage.html')

def profile(request):
    return render(request, 'profilePage.html')

def hobby(request):
    return render(request, 'hobbyPage.html')

def education(request):
    return render(request, 'educationPage.html')

def sosmed(request):
    return render(request, 'socialmediaPage.html')

def contact(request):
    return render(request, 'contactmePage.html')

def unisub(request):
    if request.method == "POST":
        form = matkulForm(request.POST)
        if form.is_valid():
            # formkuliah = Matkul()
            # formkuliah.mata_kuliah = form.cleaned_data['mata_kuliah']
            # formkuliah.dosen = form.cleaned_data['dosen']
            # formkuliah.sks = form.cleaned_data['sks']
            # formkuliah.tahun = form.cleaned_data['tahun']
            # formkuliah.kelas = form.cleaned_data['kelas']
            u = Matkul.objects.create(mata_kuliah=request.POST['mata_kuliah'],dosen=request.POST['dosen'],sks=request.POST['sks'],tahun=request.POST['tahun'],kelas=request.POST['kelas'])
            u.save()
        return redirect('/unisub')
    else:
        matakuliah = Matkul.objects.all()
        form = matkulForm()
        response = {"matakuliah":matakuliah, 'form' : form}
        return render(request,'unisub.html',response)


def deletematkul(request,pk):
    if request.method == 'POST':
        form = matkulForm(request.POST)
        if form.is_valid():
            formkuliah = Matkul()
            formkuliah.mata_kuliah = form.cleaned_data['mata_kuliah']
            formkuliah.dosen = form.cleaned_data['dosen']
            formkuliah.sks = form.cleaned_data['sks']
            formkuliah.tahun = form.cleaned_data['tahun']
            formkuliah.kelas = form.cleaned_data['kelas']
            formkuliah.save()
        return redirect('/unisub')
    else:
        Matkul.objects.filter(pk=pk).delete()
        matakuliah = Matkul.objects.all()
        form = matkulForm()        

    response = {'form':form, 'matakuliah':matakuliah}
    return render(request, 'unisub.html', response) 


      
